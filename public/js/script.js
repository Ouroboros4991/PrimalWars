var polygonAction = function polygonAction(){
        var id = $(this).attr('id');
        console.log(id);
};

var ajaxCall = function(requestURL){
    $.ajax({
        url:requestURL
    }).done(function(data){
        $('.ui-content').html(data);
    });
};

var loaded = false;

$(function(){
    console.log("Script is loaded");
    if(!loaded){
        ajaxCall("village");
    }
    //$('#grid').append(hexagonModule.createGrid(7));
    $('polygon').on("click",polygonAction);
    $('.hexLink').on("click",function(event){
        event.preventDefault();
        var url = $(this).attr('id');
        ajaxCall(url);
    });
});
