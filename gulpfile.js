var gulp = require("gulp");
var nodemon = require("gulp-nodemon");
var sass = require("gulp-sass");

gulp.task('sass',function(){
    return gulp.src('src/styles/main.scss')
            .pipe(sass().on('error',sass.logError))
            .pipe(gulp.dest('public/css'));
});

gulp.task('default',['sass'],function(){
    return nodemon({
        script:'app.js',
        ext: '.js .scss .ejs',
        env:{
            'PORT':8000
        },
        tasks:['sass']
    }).on('restart',function(){
        console.log("Restarting server");
    });
});
