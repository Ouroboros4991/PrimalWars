var express = require("express");
var app = express();

var port = process.env.PORT || 8000;

app.use(express.static('public'));
app.set('views','./src/views');
app.set('view engine','ejs');

app.get('/',function(request,response){
    response.render('index',{
    });
});

app.get('/fields',function(request,response){
    response.render('fields',{
    });
});
app.get('/village',function(request,response){
    response.render('village',{
    });
});
app.get('/map',function(request,response){
    response.render('map',{
    });
});app.get('/rang',function(request,response){
    response.render('rang',{
    });
});app.get('/reports',function(request,response){
    response.render('reports',{
    });
});app.get('/messages',function(request,response){
    response.render('messages',{
    });
});

app.listen(port,function(err){
    console.log("Server is running");
});
